import React, { useState, useEffect } from 'react';
import TypeAnimation from 'react-type-animation';

const Header = ({basicsInfo, works, projects, achievements, curMode, setCurMode}) => {
	const [modes] = useState(getDefaultModes(works, projects, achievements));
	const [screenMode, setScreenModes] = useState(document.body.clientWidth > 450 ? 0 : 1);
	const [fillerMode, setFillerMode] = useState(modes[0] || "");

	const [widths] = useState([
		getDefaultWidths(modes),
		getMiniWidths(modes)
	]);
	const [lefts] = useState([
		getDefaultLefts(widths[0]),
		getDefaultLefts(widths[1])
	]);
	const [switchWidths] = useState([
		getDefaultSwitchWidth(widths[0]),
		getDefaultSwitchWidth(widths[1])
	]);

	useEffect(() => {
		setFillerMode(curMode);
	}, [])

	useEffect(() => {
		if (modes[0]) setCurMode(modes[0]);

		window.addEventListener('resize', () => {
			setScreenModes(document.body.clientWidth > 450 ? 0 : 1)
		});
	}, []);

	return (
		<div id="header" >
			<h1 className="header-title">
				{basicsInfo.firstName + " " + basicsInfo.lastName}
			</h1>
			<div className="header-desc">
				<TypeAnimation
					cursor={false}
					sequence={[basicsInfo.label, 1000]}
					wrapper="p"
				/>

			</div>

			{
				modes.length != 0 && basicsInfo.fulltimeResume != "true" &&
				<div
					className="header-switch"
					style={{
						width: switchWidths[screenMode]
					}}
				>
					<span
						className="header-switcher"
						style={{
							transform: `translateX(${lefts[screenMode][modes.indexOf(fillerMode)]}px)`,
							width: widths[screenMode][modes.indexOf(fillerMode)]
						}}
					/>

					{
						modes.map((mode, index) => (
							<button
								className={`header-switch-btn ${(fillerMode == mode) ? "header-switch-btn-active" : ""}`}
								key={index}
								onClick={e => {
									e.stopPropagation();
									setCurMode(mode);
								}}
								onMouseEnter={e => setFillerMode(mode)}
								onMouseLeave={e => setFillerMode(curMode)}
								style={{
									top: 0,
									left: lefts[screenMode][index],
									width: widths[screenMode][index]
								}}
							>
								{mode}
							</button>
						))
					}
				</div>
			}
		</div>
	);
};

function getDefaultModes(works, projects, achievements) {
	const modes = [];

	if (works.length != 0) modes.push("experience");
	if (projects.length != 0) modes.push("freelance");
	if (achievements.length != 0) modes.push("achievements");

	return modes;
}

function getDefaultWidths(modes) {
	const defWidths = []

	if (modes.indexOf("experience") != -1) defWidths.push(135);
	if (modes.indexOf("freelance") != -1) defWidths.push(135);
	if (modes.indexOf("achievements") != -1) defWidths.push(135);

	return defWidths;
}

function getMiniWidths(modes) {
	const miniWidths = []

	if (modes.indexOf("experience") != -1) miniWidths.push(100);
	if (modes.indexOf("freelance") != -1) miniWidths.push(100);
	if (modes.indexOf("achievements") != -1) miniWidths.push(100);

	return miniWidths;
}

function getDefaultSwitchWidth(widths) {
	let switchWidth = 0;

	for (let i = 0; i < widths.length; i++) switchWidth += widths[i];

	return switchWidth;
}

function getDefaultLefts(widths) {
	const defLefts = [0];

	for (let i = 0; i < widths.length - 1; i++) defLefts[i + 1] = defLefts[i] + widths[i];

	return defLefts;
}

export default Header;