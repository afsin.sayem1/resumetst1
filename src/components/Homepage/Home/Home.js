import React, { useEffect, useState } from "react";
import Header from "../Header/Header";
import Navbar from "../Navbar/Navbar";
import Work from "../Work/Work";
import Education from "../Education/Education";
import Skills from "../Skills/Skills";

export default function Home({
  resumeData,
  colorData,
  handleToggleDrawer,
  setColorSchemas,
}) {
  const [curWork, setCurWork] = useState(0);
  const [curProj, setCurProj] = useState(0);
  const [curAchi, setCurAchi] = useState(0);
  const [curColorSchema, setCurColorSchema] = useState(0);

  const [prevWork, setPrevWork] = useState(0);
  const [prevProj, setPrevProj] = useState(0);
  const [prevAchi, setPrevAchi] = useState(0);
  const [prevColorSchema, setPrevColorSchema] = useState(curColorSchema);

  const [nextWork, setNextWork] = useState(0);
  const [nextProj, setNextProj] = useState(0);
  const [nextAchi, setNextAchi] = useState(0);
  const [nextColorSchema, setNextColorSchema] = useState(curColorSchema);

  const [colorSchema, setColorSchema] = useState([
    ["#f96161", "#fce77d"],
    ["#fce77d", "#f96161"],
    ["#f9d342", "#292826"],
    ["#292826", "#f9d342"],
    ["#df678c", "#4C167A"],
    ["#4C167A", "#df678c"],
    ["#ccf381", "#4831d4"],
    ["#4831d4", "#ccf381"],
    ["#4a274f", "#f0a07c"],
    ["#f0a07c", "#4a274f"],
    ["#fad744", "#ef5455"],
    ["#ef5455", "#fad744"],
    ["#fff748", "#3c1a5b"],
    ["#3c1a5b", "#fff748"],
    ["#2f3c7e", "#fbeaeb"],
    ["#fbeaeb", "#2f3c7e"],
    ["#ec4d37", "#1d1b1b"],
    ["#1d1b1b", "#ec4d37"],
    ["#8bd8bd", "#243665"],
    ["#243665", "#8bd8bd"],
    ["#141a46", "#ec8b5e"],
    ["#243665", "#ec8b5e"],
    ["#1F2029", "#ffe67c"],
    ["#ffe67c", "#1F2029"],
    ["#f4a950", "#161b21"],
    ["#161b21", "#f4a950"],
    ["#eb2188", "#080a52"],
    ["#080a52", "#eb2188"],
    ["#4a171e", "#EFEFEF"],
    ["#EFEFEF", "#4a171e"],
    ["#d2302c", "#f7f7f9"],
    ["#f7f7f9", "#d2302c"],
    ["#358597", "#f4a896"],
    ["#f4a896", "#358597"],
    ["#262223", "#ddc6b6"],
    ["#ddc6b6", "#262223"],
    ["#234e70", "#fbf8be"],
    ["#fbf8be", "#234e70"],
    ["#f7c5cc", "#cc313d"],
    ["#cc313d", "#f7c5cc"],
    ["#e2d3f4", "#013dc4"],
    ["#013dc4", "#e2d3f4"],
    ["#ee4e34", "#fcedda"],
    ["#ee4e34", "#e2d3f4"],
    ["#96351e", "#dbb98f"],
    ["#dbb98f", "#96351e"],
    ["#5F4B8B", "#E69A8D"],
    ["#E69A8D", "#5F4B8B"],
    ["#000000FF", "#FFFFFFFF"],
    ["#FFFFFFFF", "#000000FF"],
    ["#00203FFF", "#ADEFD1FF"],
    ["#ADEFD1FF", "#00203FFF"],
    ["#606060FF", "#D6ED17FF"],
    ["#D6ED17FF", "#606060FF"],
    ["#2C5F2D", "#97BC62FF"],
    ["#97BC62FF", "#2C5F2D"],
    ["#89ABE3FF", "#FCF6F5FF"],
    ["#FCF6F5FF", "#89ABE3FF"],
    ["#FCF951FF", "#422057FF"],
    ["#422057FF", "#FCF951FF"],
    ["#00B1D2FF", "#FDDB27FF"],
    ["#FDDB27FF", "#00B1D2FF"],
  ]);

  const [curMode, setCurMode] = useState("experience");

  useEffect(() => {
    if (curWork == nextWork || curWork == prevWork) {
      if (curWork + 1 in resumeData.work) {
        setNextWork(curWork + 1);
      } else if (0 in resumeData.work && curWork != 0) {
        setNextWork(0);
      }

      if (curWork - 1 in resumeData.work) {
        setPrevWork(curWork - 1);
      } else if (resumeData.work.length > 1) {
        setPrevWork(resumeData.work.length - 1);
      }
    }
  }, [prevWork, curWork, nextWork]);

  useEffect(() => {
    if (curProj == nextProj || curProj == prevProj) {
      if (curProj + 1 in resumeData.projects) {
        setNextProj(curProj + 1);
      } else if (0 in resumeData.projects && curProj != 0) {
        setNextProj(0);
      }

      if (curProj - 1 in resumeData.projects) {
        setPrevProj(curProj - 1);
      } else if (resumeData.projects.length > 1) {
        setPrevProj(resumeData.projects.length - 1);
      }
    }
  }, [prevProj, curProj, nextProj]);

  useEffect(() => {
    if (curAchi == nextAchi || curAchi == prevAchi) {
      if (curAchi + 1 in resumeData.achievements) {
        setNextAchi(curAchi + 1);
      } else if (0 in resumeData.achievements && curAchi != 0) {
        setNextAchi(0);
      }

      if (prevAchi - 1 in resumeData.achievements) {
        setPrevAchi(curAchi - 1);
      } else if (resumeData.achievements.length > 1) {
        setPrevAchi(resumeData.achievements.length - 1);
      }
    }
  }, [prevAchi, curAchi, nextAchi]);

  useEffect(() => {
    if (
      curColorSchema == nextColorSchema ||
      curColorSchema == prevColorSchema
    ) {
      if (curColorSchema + 1 in colorData) {
        setNextColorSchema(curColorSchema + 1);
        setColorSchemas(curColorSchema + 1);
      } else if (0 in colorData && curColorSchema != 0) {
        setNextColorSchema(0);
        setColorSchemas(0);
      }

      if (curColorSchema - 1 in colorData) {
        setPrevColorSchema(curColorSchema - 1);
      } else if (colorData.length > 1) {
        setPrevColorSchema(colorData.length - 1);
      }
    }
  }, [prevColorSchema, curColorSchema, nextColorSchema]);

  return (
    <div
      id="home"
      onClick={
        curMode == "experience"
          ? () => {
              setCurWork(nextWork);
              setCurColorSchema(nextColorSchema);
            }
          : curMode == "freelance"
          ? () => {
              setCurProj(nextProj);
              setCurColorSchema(nextColorSchema);
            }
          : () => {
              setCurAchi(nextAchi);
              setCurColorSchema(nextColorSchema);
            }
      }
    >
      <Navbar
        headerContacts={resumeData.basics.headerContacts}
        barContacts={resumeData.basics.barContacts}
        handleToggleDrawer={handleToggleDrawer}
      />

      <Header
        basicsInfo={resumeData.basics}
        works={resumeData.work}
        achievements={resumeData.achievements}
        projects={resumeData.projects}
        curMode={curMode}
        setCurMode={setCurMode}
      />

      <div id="main">
        <div className="work-section">
          <Work
            work={curWork in resumeData.work ? resumeData.work[curWork] : {}}
            achievement={
              curAchi in resumeData.achievements
                ? resumeData.achievements[curAchi]
                : {}
            }
            project={
              curProj in resumeData.projects ? resumeData.projects[curProj] : {}
            }
            downloadLinks={
              resumeData.basics.fulltimeResume == "true"
                ? resumeData.downloadFulltime
                : resumeData.downloadFreelance
            }
            curMode={curMode}
            handleNextClick={
              curMode == "experience"
                ? (e) => {
                    e.stopPropagation();
                    setCurWork(nextWork);
                    setCurColorSchema(nextColorSchema);
                  }
                : curMode == "freelance"
                ? (e) => {
                    e.stopPropagation();
                    setCurProj(nextProj);
                    setCurColorSchema(nextColorSchema);
                  }
                : (e) => {
                    e.stopPropagation();
                    setCurAchi(nextAchi);
                    setCurColorSchema(nextColorSchema);
                  }
            }
            handlePrevClick={
              curMode == "experience"
                ? (e) => {
                    e.stopPropagation();
                    setCurWork(prevWork);
                    setCurColorSchema(prevColorSchema);
                  }
                : curMode == "freelance"
                ? (e) => {
                    e.stopPropagation();
                    setCurProj(prevProj);
                    setCurColorSchema(prevColorSchema);
                  }
                : (e) => {
                    e.stopPropagation();
                    setCurAchi(prevAchi);
                    setCurColorSchema(prevColorSchema);
                  }
            }
          />
        </div>

        <div className="educ-section">
          <Education education={resumeData.education} />
        </div>

        <div className="skills-section">
          <Skills skills={resumeData.skills} />
        </div>
      </div>

      <style>
        {`
					.nav-link,
					.nav-link span,
					.nav-link svg,

					.header-title,
					.header-desc,
					.header-switch-btn,

					.work-link,

					.skills-title,
					.skill-subtitle,
					.skill-desc,

					.educ-title,
					.educ-chose-item-button {
						color: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][1]
                : colorSchema[0][1]
            };
					}

					.work-link,
					.educ-choose-choosed {
						border-color: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][1]
                : colorSchema[0][1]
            };
					}

					#home,
					#work,
					.header-switch {
						background: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][0]
                : colorSchema[0][0]
            };
					}

					.work-link:hover,
					.header-switch-btn-active {
						color: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][0]
                : colorSchema[0][0]
            };
					}

					.work-link::before,
					.header-switcher {
						background: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][1]
                : colorSchema[0][1]
            };
					}

					.header-switch {
						border: 1px solid ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][1]
                : colorSchema[0][1]
            };
					}

					.work-left-btn,
					.work-right-btn {
						border-color: ${
              colorData[curColorSchema] in colorSchema
                ? colorSchema[colorData[curColorSchema]][1]
                : colorSchema[0][1]
            };
					}
				`}
      </style>
    </div>
  );
}

function getDefaultColorSchema(colorData) {
  const count = Number(
    window.location.hash.slice(1, window.location.hash.length)
  );

  if (count in colorData) {
    return count;
  } else {
    window.location.hash = "#0";
    return 0;
  }
}
