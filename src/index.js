import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// import resumeData from './data/data.json';
// import colorData from './data/color.json';

// const hashData = window.location.hash;

// const endColorData =  hashData.length != 0 ?
//   [Number(hashData.slice(1, hashData.length))] :
//   colorData;

// const basicSimple = localStorage.getItem("basic-simple");
// const headerContacts = localStorage.getItem("HCA");
// const barContacts = localStorage.getItem("BCA");

// const downloadFulltime = localStorage.getItem('downloadFulltime');
// const downloadFreelance = localStorage.getItem('downloadFreelance');

// const audio = localStorage.getItem('audio');
// const education = localStorage.getItem('educ');
// const skills = localStorage.getItem('skills');

// const work = localStorage.getItem('work');
// const achievements = localStorage.getItem('achi');
// const projects = localStorage.getItem('proj');


// if (basicSimple) {
//   const data = JSON.parse(basicSimple);

//   resumeData.basics = {
//     ...resumeData.basics,
//     ...data
//   }
// }

// if (headerContacts) {
//   const data = JSON.parse(headerContacts);
//   resumeData.headerContacts = data.map((dataItem) => {
//     const newDataItem = {...dataItem};
//     delete(newDataItem.key)
//     return newDataItem;
//   });
// }

// if (barContacts) {
//   const data = JSON.parse(barContacts);
//   resumeData.barContacts = data.map((dataItem) => {
//     const newDataItem = {...dataItem};
//     delete(newDataItem.key)
//     return newDataItem;
//   });
// }

// if (downloadFulltime) {
//   const data = JSON.parse(downloadFulltime);

//   resumeData.downloadFulltime = [
//     {
//       name: data.FDN,
//       link: data.FL
//     },
//     {
//       name: data.SDN,
//       link: data.SL
//     }
//   ]
// }

// if (downloadFreelance) {
//   const data = JSON.parse(downloadFreelance);

//   resumeData.downloadFulltime = [
//     {
//       name: data.FDN,
//       link: data.FL
//     },
//     {
//       name: data.SDN,
//       link: data.SL
//     }
//   ]
// }

// if (audio) {
//   const data = JSON.parse(audio);

//   resumeData.additionalAudio = {
//     popUpOrHeaderAudio: data.pop,
//     skillsAudio: data.skills,
//     educationAudio: data.educ
//   }
// }

// if (education) {
//   const data = JSON.parse(education);
//   resumeData.education = data.map((dataItem) => {
//     const newDataItem = {...dataItem};
//     delete(newDataItem.key)
//     return newDataItem;
//   });
// }

// if (skills) {
//   const data = JSON.parse(skills);
//   resumeData.skills = data.map(dataItem => {
//     return {
//       name: dataItem.name,
//       keywords: dataItem.keywords.map(keyword => (keyword.str))
//     }
//   });
// }

// if (work) {
//   const data = JSON.parse(work);
//   resumeData.work = data.map(dataItem => {
//     const newDataItem = {
//       ...dataItem
//     }
//     delete(newDataItem.key);
//     newDataItem.highlights = newDataItem.highlights.map(highlight => (highlight.str));

//     return newDataItem;
//   });
// }

// if (achievements) {
//   const data = JSON.parse(achievements);
//   resumeData.achievements = data.map((dataItem) => {
//     const newDataItem = {...dataItem};
//     delete(newDataItem.key);

//     return newDataItem;
//   });
// }

// if (projects) {
//   const data = JSON.parse(projects);
//   resumeData.projects = data.map(dataItem => {
//     const newDataItem = {...dataItem};
//     delete(newDataItem.key);
//     newDataItem.highlights = newDataItem.highlights.map(highlight => (highlight.str));

//     return newDataItem;
//   });
// }

// setInterval(() => {
//   if (
//     localStorage.getItem('basic-simple') != basicSimple ||
//     localStorage.getItem('HCA') != headerContacts ||
//     localStorage.getItem('BCA') != barContacts ||

//     localStorage.getItem('downloadFulltime') != downloadFulltime ||
//     localStorage.getItem('downloadFreelance') != downloadFreelance ||

//     localStorage.getItem('audio') != audio ||
//     localStorage.getItem('educ') != education ||
//     localStorage.getItem('skills') != skills ||

//     localStorage.getItem('work') != work ||
//     localStorage.getItem('achi') != achievements ||
//     localStorage.getItem('proj') != projects
//   ) window.location.reload();
// }, 5000);

// window.addEventListener('hashchange', () => {
//   window.location.reload();
// });

const dataFetch = fetch('./data/data.json', {cache: "no-cache"});
const colorFetch = fetch('./data/color.json', {cache: "no-cache"});

Promise.all([
  dataFetch,
  colorFetch
]).then(fetchedData => {
  Promise.all([
    fetchedData[0].json(),
    fetchedData[1].json()
  ]).then(readedData => {
    const resumeData = readedData[0];
    const colorData = readedData[1];

    ReactDOM.render(
      <App
        resumeData = {resumeData}
        colorData = {colorData}
      />,
      document.getElementById('root')
    );
  })
});

      // colorData = {endColorData}
